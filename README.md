# temperature reading system 

## summary


This project allows to sense the temperature through the BME280 sensor and display it on an LCD screen and publish the temperature value to an AWS server via mqtt in an interval of 5 and 30 seconds respectively.

The screen will allow an interaction through a tactile button that will allow to toggle a gpio (in this case the gpio pg13) at any time

## Project requirements

- This will be a temperature reading system.
- There will be two interfaces, AWS website, and STM32's LCD.
- The LCD will show the current temperature (every 5 seconds).
- The LCD will have a button that will turn on/off a GPIO 	(anytime).
- The AWS website will show the current temperature (every 30 seconds).
- Create the README file including all the required information about the project.
- Commit all the changes you have made (not only one, and using the prefixes standards).
- Create the required branches (using the prefixes standards)

## settings

- connect the temperature sensor to SPI3 and the cs to pin PG9

- connect ESP GPIO16 and GPIO17 pins to PC12 and PD2 pins to establish uart communication

- join the grounds and connect the ESP32 and the STM with their respective power cables

- configure pin pg13 as output labeled LD3

###  These configurations are required since the base code is taken from practice titoma fw 15

-   we set timer 2 as pwm on channel 1 with a counter period (ARR) of 100
-   we configure the spi 3 in full duplex master mode
-   we configure the husart1 as interrupt
-   connect the PA5 pin to the PG14 led to see the change in the pwm
## necessary elements

-   sensor BME280
-   STM32F429ZI discovery board
-   ESP-WROOM-32

## setting IOT

connect the esp to the stm board in order to establish communication with the cloud server

make the suggested configurations in the esp32 folder

## uart parse commands

a command analyzer was implemented via uart communication, which allows responding to commands both from the pc using the stm32 board programming cable and from uart 5 of the board for communication with the esp32.

### note 

make sure that the connection cables from the esp32 to the stm32 are well connected or do not generate interference to avoid problems with server requests.

### commands 

-   Get temperature  "*GT#"
-   Get fan speed    "*GS#"
-   Set fan speed    "*SSx#"
-   Close door      "*SD0#"
-   Open door       "*SD1#"
-   Get door state  "*GD#"
-   Get FW Version  "*GF#"
-   Get unit time   "*GU#"
-   Get heater state "*GH#"


## libraries used

    uart_driver.c
    ring_buffer.c
    command_parse.c
    sensor_driver.c

## description of libraries

### uart_driver.c

It handles incoming commands, collects it and sends it to the parser.

### ring_buffer.c

implements a ring buffer and allows its management with functions such as adding data, obtaining data and knowing how many elements are in the buffer, thus facilitating information management

### sensor_driver.c

It allows us to initialize and configure the temperature sensor, it also allows us to obtain the temperature values of the sensor.

### command_parse.c

This library is in charge of analyzing a command that is sent through a ring buffer and executing the necessary function for that command. In case the command is not known, it will send a response "*NACK#"


