#include <gui/model/Model.hpp>
#include <gui/model/ModelListener.hpp>

#include "stm32f4xx_hal.h"
#include "main.h"
#include "stdint.h"
#include <stdio.h>
#include <string.h>
#include "math.h"

//We import global variables and structures to work with information and peripherals
extern RTC_HandleTypeDef hrtc;
extern double g_measured_temperature;
extern UART_HandleTypeDef huart5;
extern UART_HandleTypeDef huart1;
extern uint8_t g_pwm_speed;
extern uint8_t g_state_heater;
extern uint8_t g_state_door;
extern UART_HandleTypeDef *uart_print;
//we initialize the variables to obtain the time in the rtc
RTC_TimeTypeDef g_rtc_time_display;
RTC_DateTypeDef g_rtc_date_display;
//we initialize the counters to keep the time between the updates of
//the temperature on the screen and the server
uint8_t tempreture_update_counter=0;
uint8_t mqtt_status_update_counter=0;
//We initialize the buffers to start communication with the esp32
char menaseje[20];
char pre_mensage[20];

/**
*@note Due to the fact that the printf and sprintf functions generate 
*problems in the combination, the functions for converting float to string 
*from the page:
*https://barcelonageeks.com/convierta-un-numero-de-punto-flotante-en-una- string-en-c/
*/

/**
 * @brief reverses a string of x length
 * 
 * @param str chain to reverse
 * @param len chain size
 * @retval none
 */
void reverse(char* str, int len)
{
    int i = 0, j = len - 1, temp;
    while (i < j) {
        temp = str[i];
        str[i] = str[j];
        str[j] = temp;
        i++;
        j--;
    }
}
/**
 * @brief converts an integer to a string
 * 
 * @param decimal integer to convert
 * @param str array in which the value is returned
 * @param len_char  number of digits to display
 * @retval returned string size
 */

int intToStr(int decimal, char str[], int len_char)
{
    int i = 0;
    while (decimal) {
        str[i++] = (decimal % 10) + '0';
        decimal = decimal / 10;
    }

    // If number of digits required is more, then
    // add 0s at the beginning
    while (i < len_char)
        str[i++] = '0';

    reverse(str, i);
    str[i] = '\0';
    return i;
}
/**
 * @brief converts an float to a string
 * 
 * @param float_number float to convert
 * @param res array in which the value is returned
 * @param afterpoint  number of data after the sign
 * @retval none
 */
void ftoa(float float_number, char* res, int afterpoint)
{
    // Extract integer part
    int ipart = (int)float_number;

    // Extract floating part
    float fpart = float_number - (float)ipart;

    // convert integer part to string
    int i = intToStr(ipart, res, 0);

    // check for display option after point
    if (afterpoint != 0) {
        res[i] = '.'; // add dot

        // Get the value of fraction part upto given no.
        // of points after dot. The third parameter
        // is needed to handle cases like 233.007
        fpart = fpart * pow(10, afterpoint);

        intToStr((int)fpart, res + i + 1, afterpoint);
    }
}

Model::Model() : modelListener(0)
{

}

void Model::tick()
{
    //we get the value of seconds from the rtc
	HAL_RTC_GetTime(&hrtc, &g_rtc_time_display, RTC_FORMAT_BIN);
	HAL_RTC_GetDate(&hrtc, &g_rtc_date_display, RTC_FORMAT_BIN);
    //We check if 5 seconds have elapsed since the last time the temperature was refreshed
	if(g_rtc_time_display.Seconds==(tempreture_update_counter+5)%60)
	{
		tempreture_update_counter=(tempreture_update_counter+5)%60;
		modelListener->set_temperature(g_measured_temperature);
	}
    //We check if 30 seconds have passed since the last time the temperature was refreshed on the server
	if (g_rtc_time_display.Seconds==(mqtt_status_update_counter+30)%60)
	{
		mqtt_status_update_counter=(mqtt_status_update_counter+30)%60;
		ftoa((float)g_measured_temperature, pre_mensage, 2);
		int len=sprintf(menaseje, "*T%s#\r\n", pre_mensage);
		HAL_UART_Transmit(&huart5, (uint8_t *)menaseje, len, 10);
	}
}
