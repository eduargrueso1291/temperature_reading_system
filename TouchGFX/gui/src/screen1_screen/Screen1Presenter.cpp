#include <gui/screen1_screen/Screen1View.hpp>
#include <gui/screen1_screen/Screen1Presenter.hpp>

Screen1Presenter::Screen1Presenter(Screen1View& v)
    : view(v)
{

}

void Screen1Presenter::activate()
{

}

void Screen1Presenter::deactivate()
{

}
/**
 * @brief this function calls the temperature update function and serves as an interface between it and the model
 * 
 * @param temperature_value temperature value to be displayed
 * @retval none
 */
void Screen1Presenter::set_temperature(double temperature)
{
	view.update_temperature(temperature);

}

