#include <gui/screen1_screen/Screen1View.hpp>

#include "stm32f4xx_hal.h"
#include "stdio.h"
#include "main.h"


extern double g_measured_temperature;

//we define the temperature at the beginning of the program
Screen1View::Screen1View()
{
	text_temperature.resizeToCurrentText();
	text_temperature.invalidate();
	Unicode::snprintfFloat(text_temperatureBuffer, TEXT_TEMPERATURE_SIZE, "%.2f", g_measured_temperature);
	text_temperature.resizeToCurrentText();
	text_temperature.invalidate();
}

void Screen1View::setupScreen()
{
    Screen1ViewBase::setupScreen();
}

void Screen1View::tearDownScreen()
{
    Screen1ViewBase::tearDownScreen();
}
/**
 * @brief this function updates the temperature value
 * 
 * @param temperature_value temperature value to be displayed
 * @retval none
 */
void Screen1View::update_temperature(double temperature_value)
{
	text_temperature.resizeToCurrentText();
	text_temperature.invalidate();
	Unicode::snprintfFloat(text_temperatureBuffer, TEXT_TEMPERATURE_SIZE, "%.2f", temperature_value);
	text_temperature.resizeToCurrentText();
	text_temperature.invalidate();
}
/**
 * @brief this function makes a toggle in a gpio port when 
 * pressing a button on the screen in this case the PG13
 * 
 * @retval none
 */
void Screen1View::toggle_gpio_function()
{
	HAL_GPIO_TogglePin(LD3_GPIO_Port, LD3_Pin);

}

