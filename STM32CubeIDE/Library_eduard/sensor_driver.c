/**
 * 
 *@file sensor_driver.c
 * @date 22/04/2023
 * @author eduar grueso
 * @version 1.1
 * @brief  In this file, the reading and processing of the data obtained from
 * the temperature sensor will be implemented.
 */


//#include "gpio.h"
//#include "spi.h"
#include "stm32f4xx_hal.h"
#include "main.h"

/*variable definition*/
extern SPI_HandleTypeDef hspi4;
// registers with the RW bit at 1 plus the 7 addressing bits
uint8_t ID_RD=0xd0;
uint8_t	RESET_RD=0xe0;
uint8_t	STATUS_RD=0xf3;
uint8_t	CTRL_MEAS_RD=0xf4;
uint8_t	CONFIG_RD=0xf5;
uint8_t	PRESS_MSB_RD=0xf7;
uint8_t	PRESS_LSB_RD=0xf8;
uint8_t	PRESS_XLSB_RD=0xf9;
uint8_t	TEMP_MSB_RD=0xfa;
uint8_t	TEMP_LSB_RD=0xfb;
uint8_t	TEMP_XLSB_RD=0xfc;

//registers with the RW bit at 0 plus the 7 addressing bits
uint8_t	ID_WT=0x50;
uint8_t	RESET_WT=0x60;
uint8_t	STATUS_WT=0x73;
uint8_t	CTRL_MEAS_WT=0x74;
uint8_t	CONFIG_WT=0x75;
uint8_t	PRESS_MSB_WT=0x77;
uint8_t	PRESS_LSB_WT=0x78;
uint8_t	PRESS_XLSB_WT=0x79;
uint8_t	TEMP_MSB_WT=0x7a;
uint8_t	TEMP_LSB=0x7b;
uint8_t	TEMP_XLSB=0x7c;
//location of the compensation registers
uint8_t dig_T1_RD=0x88;
uint8_t dig_T2_RD=0x8a;
uint8_t dig_T3_RD=0x8c;
uint8_t dig_P1_RD=0x8e;
uint8_t dig_P2_RD=0x90;
uint8_t dig_P3_RD=0x92;
uint8_t dig_P4_RD=0x94;
uint8_t dig_P5_RD=0x96;
uint8_t dig_P6_RD=0x98;
uint8_t dig_P7_RD=0x9a;
uint8_t dig_P8_RD=0x9c;
uint8_t dig_P9_RD=0x9e;
/*sensor configuration
 * osrs_t x1 001
 * osrs_p x4 011
 * mode normal 11
 * ctrs_meas 00101111
 *
 * t_sb 1000 101
 * filter x4 010
 * config 10101000
 * */
uint8_t ctrs_meas_setup=0b00101111;//configuration for the CTRL_MEAS_WT registry
uint8_t config_r_setup=0b10101000;//configuration for the CONFIG_WT registry

uint8_t dig_T1[2]={0x00,0x00};//stores the records that make up dig_T1
uint8_t dig_T2[2]={0x00,0x00};//stores the records that make up dig_T2
uint8_t dig_T3[2]={0x00,0x00};//stores the records that make up dig_T3
uint8_t dig_P1[2]={0x00,0x00};//stores the records that make up dig_P1
uint8_t dig_P2[2]={0x00,0x00};//stores the records that make up dig_P2
uint8_t dig_P3[2]={0x00,0x00};//stores the records that make up dig_P3
uint8_t dig_P4[2]={0x00,0x00};//stores the records that make up dig_P4
uint8_t dig_P5[2]={0x00,0x00};//stores the records that make up dig_P5
uint8_t dig_P6[2]={0x00,0x00};//stores the records that make up dig_P6
uint8_t dig_P7[2]={0x00,0x00};//stores the records that make up dig_P7
uint8_t dig_P8[2]={0x00,0x00};//stores the records that make up dig_P8
uint8_t dig_P9[2]={0x00,0x00};//stores the records that make up dig_P9
//compensation values
unsigned short dig_T1_full; //contains the full offset value of dig_T1
short dig_T2_full;//contains the full offset value of dig_T2
short dig_T3_full;//contains the full offset value of dig_T3
unsigned short dig_P1_full;//contains the full offset value of the dig_P1 register
short dig_P2_full;//contains the full offset value of the dig_P2 register
short dig_P3_full;//contains the full offset value of the dig_P3 register
short dig_P4_full;//contains the full offset value of the dig_P4 register
short dig_P5_full;//contains the full offset value of the dig_P5 register
short dig_P6_full;//contains the full offset value of the dig_P6 register
short dig_P7_full;//contains the full offset value of the dig_P7 register
short dig_P8_full;//contains the full offset value of the dig_P8 register
short dig_P9_full;//contains the full offset value of the dig_P9 register

uint8_t temp_registers[3]; //temperature records
uint8_t preasure_registers[3];//pressure records

int32_t temp_register_full; //combined temperature of all records
uint32_t pre_register_full; //combined pressure of all registers

double g_measured_temperature=0; //temperature compensated measurement
double g_measured_pressure=0; //compensated pressure measurement

int32_t g_t_fine;//fine temperature value is used in pressure calculation

/**
 * @brief	calculates the value of the compensated temperature
 * @note	calculates the offset for the measured raw temperature value using
 * 			the offset registers read in the respective global variables.
 * @param	None
 * @retval	double
 */

//calculates the compensated temperature value
double temperature_compensation(void)
{
	 int32_t var1, var2, T;
	 var1 = ((((temp_register_full>>3)-((int32_t)dig_T1_full<<1)))*((int32_t)dig_T2_full)) >> 11;
	 var2 = (((((temp_register_full>>4) - ((int32_t)dig_T1_full)) * ((temp_register_full>>4) - ((int32_t)dig_T1_full))) >> 12) *
	 ((int32_t)dig_T3_full)) >> 14;
	 g_t_fine = var1 + var2;
	 T = (g_t_fine * 5 + 128) >> 8;
	 return (double)T/100.0;
}

/**
 * @brief	calculates the value of the compensated pressure
 * @note	calculates the offset for the measured raw pressure value using
 * 			the offset registers read in the respective global variables.
 * @param	None
 * @retval	double
 */

double pressure_compensation(void)
{
	double var1, var2, p;
	var1 = ((double)g_t_fine/2.0)-64000.0;
	var2 = var1 * var1 * ((double)dig_P6_full) / 32768.0;
	var2 = var2 + var1 * ((double)dig_P5_full) * 2.0;
	var2 = (var2/4.0)+(((double)dig_P4_full) * 65536.0);
	var1 = (((double)dig_P3_full) * var1 * var1 / 524288.0 + ((double)dig_P2_full) * var1) / 524288.0;
	var1 = (1.0 + var1 / 32768.0)*((double)dig_P1_full);
	if (var1 == 0.0)
	{
	return 0; // avoid exception caused by division by zero
	}
	p = 1048576.0-(double)pre_register_full;
	p = (p-(var2 / 4096.0)) * 6250.0 / var1;
	var1 = ((double)dig_P9_full) * p * p / 2147483648.0;
	var2 = p * ((double)dig_P8_full) / 32768.0;
	p = p + (var1 + var2 + ((double)dig_P7_full)) / 16.0;
	return p;
}

/**
 * @brief	temperature sensor setup
 * @note	This function writes the registers for the sensor configuration using
 * 			the defined global variables ctrs_meas_setup and config_r_setup
 * @param	None
 * @retval	HAL_StatusTypeDef
 */

HAL_StatusTypeDef configure_BME280_sensor(void)
{
	HAL_GPIO_WritePin(SENSOR_CS_GPIO_Port, SENSOR_CS_Pin, 0); //
	if((HAL_SPI_Transmit(&hspi4, &CTRL_MEAS_WT, 1, 100))!=HAL_OK )
	{
		return HAL_ERROR;
	}
	if((HAL_SPI_Transmit(&hspi4, &ctrs_meas_setup, 1, 100))!=HAL_OK )
	{
		return HAL_ERROR;
	}
	if((HAL_SPI_Transmit(&hspi4, &CONFIG_WT, 1, 100))!=HAL_OK )
	{
		return HAL_ERROR;
	}
	if((HAL_SPI_Transmit(&hspi4, &config_r_setup, 1, 100))!=HAL_OK )
	{
		return HAL_ERROR;
	}
	HAL_GPIO_WritePin(SENSOR_CS_GPIO_Port, SENSOR_CS_Pin, 1);
	return HAL_OK;
}
/**
 * @brief	read temperature records
 * @note	using the commands defined at the beginning in the global variables read
 * 			the value of the registers that contain the temperature measurement
 * @param	None
 * @retval	HAL_StatusTypeDef
 */

HAL_StatusTypeDef read_temperature(void)
{
	//we send the command to read the temperature record and read in burst
	HAL_GPIO_WritePin(SENSOR_CS_GPIO_Port, SENSOR_CS_Pin, 0);
	if ((HAL_SPI_Transmit(&hspi4, &TEMP_MSB_RD, 1, 100))!=HAL_OK)
	{
		return HAL_ERROR;
	}else
	{
		HAL_SPI_Receive(&hspi4, temp_registers, 3, 100);
	}
	HAL_GPIO_WritePin(SENSOR_CS_GPIO_Port, SENSOR_CS_Pin, 1);
	return HAL_OK;
}
/**
 * @brief	read pressure records
 * @note	using the commands defined at the beginning in the global variables read
 * 			the value of the registers that contain the pressure measurement
 * @param	None
 * @retval	HAL_StatusTypeDef
 */
HAL_StatusTypeDef read_pressure(void)
{
	//We send the command to read the pressure register and we read it in a burst
	HAL_GPIO_WritePin(SENSOR_CS_GPIO_Port, SENSOR_CS_Pin, 0);
	if ((HAL_SPI_Transmit(&hspi4, &PRESS_MSB_RD, 1, 100))!=HAL_OK)
	{
		return HAL_ERROR;
	}else
	{
		HAL_SPI_Receive(&hspi4, preasure_registers, 3, 100);
	}
	HAL_GPIO_WritePin(SENSOR_CS_GPIO_Port, SENSOR_CS_Pin, 1);
	return HAL_OK;
}
/**
 * @brief	join the temperature record
 * @note	joins the temperature registers using the bit shift
 * 			and the or operator ("|")
 * @param	None
 * @retval	int32_t
 */
int32_t union_of_temperature_records(void)
{
	int32_t temperature;
	//we combine the temperature records
	temperature=(uint32_t)temp_registers[0]<<12|(uint32_t)temp_registers[1]<<4|(uint32_t)temp_registers[2]>>4;
	return temperature;
}
/**
 * @brief	join the pressure record
 * @note	joins the pressure registers using the bit shift
 * 			and the or operator ("|")
 * @param	None
 * @retval	uint32_t
 */
uint32_t pressure_register_union(void)
{
	uint32_t preassure;
	//we combine the pressure records
	preassure=(uint32_t)preasure_registers[0]<<12|(uint32_t)preasure_registers[1]<<4|(uint32_t)preasure_registers[2]>>4;
	return preassure;
}

/**
 * @brief	reads and processes the temperature records
 * @note	This function reads the temperature and price registers
 * 			and joins them to obtain the full records of the measurements
 * 			that are stored in the global variables temp_register_full
 * 			and pre_register_full.
 * @param	None
 * @retval	HAL_StatusTypeDef
 */
HAL_StatusTypeDef read_measurement_sensor_BME280(void)
{
	if(read_temperature()!=HAL_OK)
	{
		return HAL_ERROR;
	}
	if(read_pressure()!=HAL_OK)
	{
			return HAL_ERROR;
	}
	temp_register_full=union_of_temperature_records();
	pre_register_full=pressure_register_union();
	return HAL_OK;
}

/**
 * @brief	get temperature compensated
 * @note	This function reads the temperature compensation registers
 * 			and calculates the compensated temperature.
 * @param	None
 * @retval	HAL_StatusTypeDef
 */
HAL_StatusTypeDef read_compensation_parameter_t(void)
{
	//read the value of the dig_T1 register
	HAL_GPIO_WritePin(SENSOR_CS_GPIO_Port, SENSOR_CS_Pin, 0);
	if((HAL_SPI_Transmit(&hspi4, &dig_T1_RD, 1, 100))==HAL_OK)
	{
		HAL_SPI_Receive(&hspi4, dig_T1, 2, 100);
	}else
	{
		return HAL_ERROR;
	}
	HAL_GPIO_WritePin(SENSOR_CS_GPIO_Port, SENSOR_CS_Pin, 1);
	//read the value of the dig_T2 register
	HAL_GPIO_WritePin(SENSOR_CS_GPIO_Port, SENSOR_CS_Pin, 0);
	if((HAL_SPI_Transmit(&hspi4, &dig_T2_RD, 1, 100))==HAL_OK)
	{
		HAL_SPI_Receive(&hspi4, dig_T2, 2, 100);
	}else
	{
		return HAL_ERROR;
	}
	HAL_GPIO_WritePin(SENSOR_CS_GPIO_Port, SENSOR_CS_Pin, 1);
	//read the value of the dig_T3 register
	HAL_GPIO_WritePin(SENSOR_CS_GPIO_Port, SENSOR_CS_Pin, 0);
	if((HAL_SPI_Transmit(&hspi4, &dig_T3_RD, 1, 100))==HAL_OK)
	{
		HAL_SPI_Receive(&hspi4, dig_T3, 2, 100);
	}else
	{
		return HAL_ERROR;
	}
	HAL_GPIO_WritePin(SENSOR_CS_GPIO_Port, SENSOR_CS_Pin, 1);
	//we join the offset records into a signed or unsigned int
	dig_T1_full=(uint16_t)dig_T1[1]<<8|(uint16_t)dig_T1[0];
	dig_T2_full=(int16_t)dig_T2[1]<<8|(int16_t)dig_T2[0];
	dig_T3_full=(int16_t)dig_T3[1]<<8|(int16_t)dig_T3[0];
	g_measured_temperature=temperature_compensation();
	return HAL_OK;
}
/**
 * @brief	get pressure compensated
 * @note	This function reads the pressure compensation registers
 * 			and calculates the compensated pressure.
 * @param	None
 * @retval	HAL_StatusTypeDef
 */
HAL_StatusTypeDef read_compensation_parameter_p(void)
{
	//read the value of the dig_P1 register
	HAL_GPIO_WritePin(SENSOR_CS_GPIO_Port, SENSOR_CS_Pin, 0);
	if((HAL_SPI_Transmit(&hspi4, &dig_P1_RD, 1, 100))==HAL_OK)
	{
		HAL_SPI_Receive(&hspi4, dig_P1, 2, 100);
	}else
	{
		return HAL_ERROR;
	}
	HAL_GPIO_WritePin(SENSOR_CS_GPIO_Port, SENSOR_CS_Pin, 1);
	//read the value of the dig_P2 register
	HAL_GPIO_WritePin(SENSOR_CS_GPIO_Port, SENSOR_CS_Pin, 0);
	if((HAL_SPI_Transmit(&hspi4, &dig_P2_RD, 1, 100))==HAL_OK)
	{
		HAL_SPI_Receive(&hspi4, dig_P2, 2, 100);
	}else
	{
		return HAL_ERROR;
	}
	HAL_GPIO_WritePin(SENSOR_CS_GPIO_Port, SENSOR_CS_Pin, 1);
	//read the value of the dig_P3 register
	HAL_GPIO_WritePin(SENSOR_CS_GPIO_Port, SENSOR_CS_Pin, 0);
	if((HAL_SPI_Transmit(&hspi4, &dig_P3_RD, 1, 100))==HAL_OK)
	{
		HAL_SPI_Receive(&hspi4, dig_P3, 2, 100);
	}else
	{
		return HAL_ERROR;
	}
	HAL_GPIO_WritePin(SENSOR_CS_GPIO_Port, SENSOR_CS_Pin, 1);
	//read the value of the dig_P4 register
	HAL_GPIO_WritePin(SENSOR_CS_GPIO_Port, SENSOR_CS_Pin, 0);
	if((HAL_SPI_Transmit(&hspi4, &dig_P4_RD, 1, 100))==HAL_OK)
	{
		HAL_SPI_Receive(&hspi4, dig_P4, 2, 100);
	}else
	{
		return HAL_ERROR;
	}
	HAL_GPIO_WritePin(SENSOR_CS_GPIO_Port, SENSOR_CS_Pin, 1);
	//read the value of the dig_P5 register
	HAL_GPIO_WritePin(SENSOR_CS_GPIO_Port, SENSOR_CS_Pin, 0);
	if((HAL_SPI_Transmit(&hspi4, &dig_P5_RD, 1, 100))==HAL_OK)
	{
		HAL_SPI_Receive(&hspi4, dig_P5, 2, 100);
	}else
	{
		return HAL_ERROR;
	}
	HAL_GPIO_WritePin(SENSOR_CS_GPIO_Port, SENSOR_CS_Pin, 1);
	//read the value of the dig_P6 register
	HAL_GPIO_WritePin(SENSOR_CS_GPIO_Port, SENSOR_CS_Pin, 0);
	if((HAL_SPI_Transmit(&hspi4, &dig_P6_RD, 1, 100))==HAL_OK)
	{
		HAL_SPI_Receive(&hspi4, dig_P6, 2, 100);
	}else
	{
		return HAL_ERROR;
	}
	HAL_GPIO_WritePin(SENSOR_CS_GPIO_Port, SENSOR_CS_Pin, 1);
	//read the value of the dig_P7 register
	HAL_GPIO_WritePin(SENSOR_CS_GPIO_Port, SENSOR_CS_Pin, 0);
	if((HAL_SPI_Transmit(&hspi4, &dig_P7_RD, 1, 100))==HAL_OK)
	{
		HAL_SPI_Receive(&hspi4, dig_P7, 2, 100);
	}else
	{
		return HAL_ERROR;
	}
	HAL_GPIO_WritePin(SENSOR_CS_GPIO_Port, SENSOR_CS_Pin, 1);
	//read the value of the dig_P7 register
	HAL_GPIO_WritePin(SENSOR_CS_GPIO_Port, SENSOR_CS_Pin, 0);
	if((HAL_SPI_Transmit(&hspi4, &dig_P8_RD, 1, 100))==HAL_OK)
	{
		HAL_SPI_Receive(&hspi4, dig_P8, 2, 100);
	}else
	{
		return HAL_ERROR;
	}
	HAL_GPIO_WritePin(SENSOR_CS_GPIO_Port, SENSOR_CS_Pin, 1);
	//read the value of the dig_P9 register
	HAL_GPIO_WritePin(SENSOR_CS_GPIO_Port, SENSOR_CS_Pin, 0);
	if((HAL_SPI_Transmit(&hspi4, &dig_P9_RD, 1, 100))==HAL_OK)
	{
		HAL_SPI_Receive(&hspi4, dig_P9, 2, 100);
	}else
	{
		return HAL_ERROR;
	}
	HAL_GPIO_WritePin(SENSOR_CS_GPIO_Port, SENSOR_CS_Pin, 1);
	//we join the records to obtain the complete compensation record
	dig_P1_full=(uint16_t)dig_P1[1]<<8|(uint16_t)dig_P1[0];
	dig_P2_full=(uint16_t)dig_P2[1]<<8|(uint16_t)dig_P2[0];
	dig_P3_full=(uint16_t)dig_P3[1]<<8|(uint16_t)dig_P3[0];
	dig_P4_full=(uint16_t)dig_P4[1]<<8|(uint16_t)dig_P4[0];
	dig_P5_full=(uint16_t)dig_P5[1]<<8|(uint16_t)dig_P5[0];
	dig_P6_full=(uint16_t)dig_P6[1]<<8|(uint16_t)dig_P6[0];
	dig_P7_full=(uint16_t)dig_P7[1]<<8|(uint16_t)dig_P7[0];
	dig_P8_full=(uint16_t)dig_P8[1]<<8|(uint16_t)dig_P8[0];
	dig_P9_full=(uint16_t)dig_P9[1]<<8|(uint16_t)dig_P9[0];
	//calculate pressure compensation
	g_measured_pressure=pressure_compensation();
	return HAL_OK;
}



