/** 
 * @file command_parse.c
 * @date 24/05/2023
 * @author eduar grueso
 * @version 1.1
 * @brief In the document we implement a series of actions depending on the command 
 * that is passed to it when calling the command parse function
 */
#include "main.h"
#include "ring_buffer.h"
#include "command_parse.h"
#include "firmware_version.h"
#include "stdio.h"
#include "sensor_driver.h"
//#include "rtc.h"
#include "stm32f4xx_hal.h"
/*Private define*/
/*Private macro*/
/*Private variable declaration*/
TIM_TypeDef *g_tim_pwm;//tim structure used for pwm
uint8_t g_chanel_pwm;//pwm output channel used
uint8_t g_state_door=DOOR_STATE_OPEN;//door status
uint8_t g_pwm_speed;//fan speed
uint8_t g_state_heater=HEATER_STATE_OFF;//heater status
RTC_TimeTypeDef g_rtc_time;//stores the rtc weather data
RTC_DateTypeDef g_rtc_date;//stores the date data of the rtc
uint8_t g_days_elapsed=0;//counter of days elapsed from 0 to 255 days
extern RTC_HandleTypeDef hrtc;
/*Private functions prototype*/
/***************************************************************************************************/

/**
 * @brief this function stores the structures and values that we use to manage the pwm
 * 
 * @param pwm_structure timer structure to use
 * @param chanel channel used for pwm
 */
void command_parse_set_tim_pwm(TIM_TypeDef *pwm_structure, uint8_t chanel)
{
	g_tim_pwm=pwm_structure;
	g_chanel_pwm=chanel;
}
/**
 * @brief Set the ccr register depending on the channel 
 * 
 * @param chanel pwm channel using 
 * @note the value must be an integer from 1 to 4
 * @param ccr_value value to save in the ccr register of the pwm
 * @retval none
 */
void set_ccr_pwm(uint8_t chanel, uint8_t ccr_value)
{
	switch(chanel)
	{
		case 1:
			g_tim_pwm->CCR1=ccr_value;
			break;
		case 2:
			g_tim_pwm->CCR2=ccr_value;
			break;
		case 3:
			g_tim_pwm->CCR3=ccr_value;
			break;
		case 4:
			g_tim_pwm->CCR4=ccr_value;
			break;
	}
}
/**
 * @brief change the value of the ccr register to change the fan speed to pwm output
 * 
 * @param speed speed value for fan
 * @note the speed value is in the range of 0 to 4 to switch between the defined speed intervals
 * @retval none
 */
void set_pwm_cooler(uint8_t speed)
{
	switch(speed)
	{
		case 0:
			set_ccr_pwm(g_chanel_pwm, 0);
			g_pwm_speed=0;
			break;
		case 1:
			set_ccr_pwm(g_chanel_pwm, 25);
			g_pwm_speed=25;
			break;
		case 2:
			set_ccr_pwm(g_chanel_pwm, 50);
			g_pwm_speed=50;
			break;
		case 3:
			set_ccr_pwm(g_chanel_pwm, 75);
			g_pwm_speed=75;
			break;
		case 4:
			set_ccr_pwm(g_chanel_pwm, 100);
			g_pwm_speed=100;
			break;
	}
}
/**
 * @brief change a character number to uint8_t
 * 
 * @param buffer_ring pointer to a structure like ring_buffer_t
 * @retval uint8_t value read in uint8_t format
 */
uint8_t read_speed_of_the_command(ring_buffer_t *buffer_ring)
{
	uint8_t final_speed_read=0;
	uint8_t speed_read;
	size_t head_buffer;
	if(buffer_ring->head<buffer_ring->tail)
	{
		head_buffer=buffer_ring->head + buffer_ring->max_size;
	}else
	{
		head_buffer=buffer_ring->head;
	}
	for (int i=buffer_ring->tail;i<head_buffer;i++)
	{

		final_speed_read=final_speed_read*10;
		ring_buffer_get(buffer_ring, &speed_read);
		final_speed_read+=speed_read-48;
	}
	return final_speed_read;
}
/**
 * @brief executes a set action depending on the command that is passed as a parameter
 * 
 * @param buffer_ring pointer to a structure like ring_buffer_t
 * @param command_character character to parse set action to execute
 * @retval none
 */
void set_command_execution(ring_buffer_t *buffer_ring, uint8_t command_character)
{
	uint16_t len_data_value=ring_buffer_size(buffer_ring);
	if (len_data_value>1 || len_data_value==0)
	{
		printf("*%s#",COMMAND_INVALID);
	}else
	{
		if (command_character==COMMAND_SPEED)
		{
			uint8_t speed_value;
			ring_buffer_get(buffer_ring, &speed_value);
			speed_value=speed_value-48;
			if(speed_value>=0 && speed_value<=4)
			{
				set_pwm_cooler(speed_value);
				printf("*%s#",COMMAND_ACEPT);
			}else
			{
				printf("*%s#",COMMAND_INVALID);
			}
		}else if(command_character==COMMAND_DOOR)
		{
			uint8_t door_state;
			ring_buffer_get(buffer_ring, &door_state);
			door_state=door_state-48;
			if (door_state==1)
			{
				g_state_door=DOOR_STATE_OPEN;
				printf("*%s#",COMMAND_ACEPT);
			}else if(door_state==0)
			{
				g_state_door=DOOR_STATE_CLOSE;
				printf("*%s#",COMMAND_ACEPT);
			}else
			{
				printf("*%s#",COMMAND_INVALID);
			}

		}else
		{
			printf("*%s#",COMMAND_INVALID);
		}
	}
}
/**
 * @brief executes a get action depending on the command that is passed as a parameter
 * 
 * @param buffer_ring pointer to a structure like ring_buffer_t
 * @param command_character character to parse get action to execute
 * @retval none
 */
void get_command_execution(ring_buffer_t *buffer_ring, uint8_t command_character)
{
	uint16_t len_data_value=ring_buffer_size(buffer_ring);
	if (len_data_value==0)
	{
		if (command_character==COMMAND_TEMPERATURE)
		{
			printf("*%c%.2f#",COMMAND_REPLY_TEMPERATURE,g_measured_temperature);
		}else if(command_character==COMMAND_SPEED)
		{
			printf("*%c%03d#",COMMAND_REPLY_SPEED,g_pwm_speed);

		}else if(command_character==COMMAND_DOOR)
		{
			printf("*%c%d#",COMMAND_REPLY_DOOR,g_state_door);
		}else if(command_character==COMMAND_VERSION)
		{
			printf("*%c%s#",COMMAND_REPLY_VERSION,FW_VERSION);
		}else if(command_character==COMMAND_HEATER)
		{
			printf("*%c%d#",COMMAND_REPLY_HEATER,g_state_heater);
		}else if(command_character==COMMAND_TIME)
		{
			HAL_RTC_GetTime(&hrtc, &g_rtc_time, RTC_FORMAT_BIN);
			HAL_RTC_GetDate(&hrtc, &g_rtc_date, RTC_FORMAT_BIN);
			printf("%c%02d:%02d:%02d:%02d",COMMAND_REPLY_TIME,g_days_elapsed,g_rtc_time.Hours,g_rtc_time.Minutes,g_rtc_time.Seconds);

		}else
		{
			printf("*%s#",COMMAND_INVALID);
		}
	}else
	{
		printf("*%s#",COMMAND_INVALID);
	}
}

/**
 * @brief parses the received command and executes a function according to the command
 *
 * @param buffer_ring pointer to a structure like ring_buffer_t
 * @retval none
 */
void command_parse(ring_buffer_t *buffer_ring)
{
	uint8_t first_character;
	uint8_t second_character;
	ring_buffer_get(buffer_ring, &first_character);
	ring_buffer_get(buffer_ring, &second_character);
	if (first_character==COMMAND_SET)
	{
		set_command_execution(buffer_ring, second_character);
	}else if(first_character==COMMAND_GET)
	{
		get_command_execution(buffer_ring, second_character);
	}else
	{
		printf("*%s#",COMMAND_INVALID);
	}
	printf("\r\n");
	ring_buffer_reset(buffer_ring);
}


/**
 * @brief Callback function for the MTC alarm to keep the count of the days elapsed
 * 
 * @param hrtc pointer pointing to a structure of type RTC_HandleTypeDef
 */
void  HAL_RTC_AlarmAEventCallback ( RTC_HandleTypeDef  * hrtc )
{
	g_days_elapsed++;
	g_rtc_date.Date=0;
	HAL_GPIO_TogglePin(LD3_GPIO_Port, LD3_Pin);
	HAL_RTC_SetDate(hrtc, &g_rtc_date, RTC_FORMAT_BIN);
}
