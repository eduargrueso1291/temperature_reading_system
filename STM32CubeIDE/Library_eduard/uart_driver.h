/** 
 * @file uart_driver.h
 * @date 22/05/2023
 * @author eduar grueso
 * @version 1.1
 * @brief receives the commands and calls the functions in charge of processing these commands
 */

#ifndef INC_UART_DRIVER_H_
#define INC_UART_DRIVER_H_
/*Exported defines********************************/
/*Exported types**********************************/
#include "ring_buffer.h"

typedef struct uart_driver_
{
	UART_HandleTypeDef *huart;
	uint8_t *rx_buffer; //buffer to configure the ring buffer
	size_t rx_len_buffer;//source buffer size
	ring_buffer_t rx_ring_buffer; //ring buffer to store the commands
	uint8_t rx_data;//stores the received data
	uint32_t ticks; 
}uart_driver_t;
/*Exported constans*******************************/
/*Exported macro**********************************/
/*Exported functions******************************/
void uart_inicialize_ring_buffer(uart_driver_t *);
void uart_driver_inicialize(uart_driver_t *driver);
void uart_driver_command(uint8_t received_data, ring_buffer_t *buffer);

#define PREAMBLE '*'
#define POSTAMBULE '#'

#endif /* INC_UART_DRIVER_H_ */
