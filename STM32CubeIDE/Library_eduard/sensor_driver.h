/**
 * 
 *@file sensor_driver.h
 * @date 22/04/2023
 * @author eduar grueso
 * @version 1.1
 * @brief  read temperature and pressure from BME280 sensor
 */

#ifndef INC_SENSOR_DRIVER_H_
#define INC_SENSOR_DRIVER_H_

/*Exported constans*******************************/
//20-bit raw temperature and price register
extern uint32_t temp_register_full;
extern uint32_t pre_register_full;
// temperature and price compensated values
extern double g_measured_temperature;
extern double g_measured_pressure;
/*Exported types**********************************/
/*Exported constans*******************************/
/*Exported macro**********************************/
/*Exported functions******************************/
double temperature_compensation(void);
double pressure_compensation(void);
HAL_StatusTypeDef configure_BME280_sensor(void);
HAL_StatusTypeDef read_measurement_sensor_BME280(void);
HAL_StatusTypeDef read_compensation_parameter_t(void);
HAL_StatusTypeDef read_compensation_parameter_p(void);
HAL_StatusTypeDef read_temperature(void);
HAL_StatusTypeDef read_pressure(void);
int32_t union_of_temperature_records(void);
uint32_t pressure_register_union(void);
/*FUNCTIONS PRTOTOTYPE END*/

#endif /* INC_SENSOR_DRIVER_H_ */
