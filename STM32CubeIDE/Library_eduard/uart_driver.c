
/** 
 * @file uart_driver.c
 * @date 22/05/2023
 * @author eduar grueso
 * @version 1.1
 * @brief This file allows you to control the reception of commands via uart and 
 * implement the process of these with the command parse library
 */
#include "main.h"
//#include "usart.h"
#include "ring_buffer.h"
#include "uart_driver.h"
#include "string.h"
#include "command_parse.h"
#include "stm32f4xx_hal.h"

extern UART_HandleTypeDef *uart_print;
extern UART_HandleTypeDef huart5;
extern UART_HandleTypeDef huart1;

uart_driver_t *g_uart_driver_structure;//uart_driver_t structure to handle the information
uint8_t g_flag_command_recieve=0; //flag to know when the message starts or ends
/**
 * @brief initialize the ring buffer with the buffer source and the buffer size
 * 
 * @param driver pointer to the structure of the uart_driver_t
 * @retval none
 */
void uart_inicialize_ring_buffer(uart_driver_t *driver)
{
	driver->rx_ring_buffer.buffer_source=driver->rx_buffer;
	driver->rx_ring_buffer.max_size=driver->rx_len_buffer;
	ring_buffer_reset(&(driver->rx_ring_buffer));
}
/**
 * @brief This function sets up the ring buffer and initiates the huart interrupt and 
 * stores the structure pointer for use globally.
 * 
 * @param driver pointer to the structure of the uart_driver_t
 * @retval none
 */
void uart_driver_inicialize(uart_driver_t *driver)
{
	uart_inicialize_ring_buffer(driver);
	HAL_UART_Receive_IT(driver->huart, &(driver->rx_data), 1);
	HAL_UART_Receive_IT(&huart5, &(driver->rx_data), 1);
	g_uart_driver_structure=driver;
}
/**
 * @brief parses the input data and detects the start and end of a command 
 * to store and call the parse command to perform the necessary action
 * 
 * @param received_data value to parse and store in ring buffer
 * @param buffer pointer to a ring buffer structure
 * @retval none
 */
void uart_driver_command(uint8_t received_data, ring_buffer_t *buffer)
{
	if(received_data==POSTAMBULE)
	{
		g_flag_command_recieve=0;
	}
	if (g_flag_command_recieve)
	{
		ring_buffer_append(buffer, received_data);
	}
	//when it finishes receiving the command it analyzes
	if(received_data==POSTAMBULE)
	{
		command_parse(buffer);
	}
	if (received_data==PREAMBLE)
	{
		g_flag_command_recieve=1;
	}
}
/**
 * @brief callback function for data reception, 
 * this function calls the uart_driver_command and restarts the huart reception interrupt
 * 
 * @param huart pointer to the UART_HandleTypeDef structure for the received huart instance
 * @retval none
 */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{

	if (huart->Instance==USART1)
	{
		uart_print=&huart1;
	}else if(huart->Instance==UART5)
	{
		uart_print=&huart5;
	}
	uart_driver_command(g_uart_driver_structure->rx_data, &(g_uart_driver_structure->rx_ring_buffer));
	HAL_UART_Receive_IT(g_uart_driver_structure->huart, &(g_uart_driver_structure->rx_data), 1);
	HAL_UART_Receive_IT(&huart5, &(g_uart_driver_structure->rx_data), 1);
}


