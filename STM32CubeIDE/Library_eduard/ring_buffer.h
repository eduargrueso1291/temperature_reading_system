/** 
 * @file ring_buffer.h
 * @date 22/05/2023
 * @author eduar grueso
 * @version 1.1
 * @brief allows handling of a ring buffer
 */

#ifndef INC_RING_BUFFER_H_
#define INC_RING_BUFFER_H_
/*Exported defines********************************/
/*Exported types**********************************/
typedef struct ring_bufer_
{
	uint8_t *buffer_source;
	size_t max_size;
	size_t head;
	size_t tail;
	uint8_t full;
}ring_buffer_t;
/*Exported constans*******************************/
/*Exported macro**********************************/
/*Exported functions******************************/
void ring_buffer_reset(ring_buffer_t *);
uint8_t ring_bufer_empty(ring_buffer_t *);
size_t ring_buffer_size(ring_buffer_t *);
uint8_t ring_buffer_get(ring_buffer_t *, uint8_t *);
void ring_buffer_append(ring_buffer_t *, uint8_t);

#endif /* INC_RING_BUFFER_H_ */
