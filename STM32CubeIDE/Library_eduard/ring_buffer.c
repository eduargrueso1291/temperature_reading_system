
/** 
 * @file ring_buffer.c
 * @date 22/05/2023
 * @author eduar grueso
 * @version 1.1
 * @brief This function stores, extracts and allows the manipulation of 
 * a ring_buffer_t structure for the handling of information
 */
#include "main.h"
#include "ring_buffer.h"
#include "stdbool.h"
/**
 * @brief this function returns the ring buffer to its initial state
 * 
 * @param buffer_ring pointer to a structure like ring_buffer_t
 * @retval none
 */
void ring_buffer_reset(ring_buffer_t *buffer_ring)
{
	buffer_ring->head=0;
	buffer_ring->tail=0;
	buffer_ring->full=0;
}
/**
 * @brief check if the ring buffer is empty
 * 
 * @param buffer_ring pointer to a structure like ring_buffer_t
 * @retval uint8_t 1 if the ring buffer is empty and 0 otherwise
 */
uint8_t ring_bufer_empty(ring_buffer_t *buffer_ring)
{
	if(buffer_ring->full==0 && buffer_ring->head==buffer_ring->tail)
	{
		return 1;
	}
	return 0;
}
/**
 * @brief returns the amount of data stored in the ring buffer
 * 
 * @param buffer_ring pointer to a structure like ring_buffer_t
 * @retval size_t data numbers stored in the ring buffer
 */
size_t ring_buffer_size(ring_buffer_t *buffer_ring)
{
	int len_buffer_ring=buffer_ring->max_size;
	if(buffer_ring->full==1)
	{
		return len_buffer_ring;
	}else if(buffer_ring->head >= buffer_ring->tail)
	{
		return (buffer_ring->head % len_buffer_ring) - (buffer_ring->tail % len_buffer_ring);
	}else if(buffer_ring->head < buffer_ring->tail)
	{
		return (buffer_ring->head % len_buffer_ring) + buffer_ring->max_size - (buffer_ring->tail % len_buffer_ring);
	}
	return 0;
}
/**
 * @brief This function stores the oldest data stored in the ring buffer, 
 * returns a 1 in case the ring buffer is empty
 * 
 * @param buffer_ring pointer to a structure like ring_buffer_t
 * @param return_data pointer to the variable in which you want to store the read value
 * @retval uint8_t returns 0 if data is returned in case the ring buffer is empty returns 1
 */
uint8_t ring_buffer_get(ring_buffer_t *buffer_ring, uint8_t *return_data)
{
	if (!ring_bufer_empty(buffer_ring))
	{
		*return_data=buffer_ring->buffer_source[buffer_ring->tail];
		buffer_ring->tail=(buffer_ring->tail + 1)% buffer_ring->max_size;
		if (buffer_ring->head==buffer_ring->tail)
		{
			buffer_ring->full=0;
		}
		return 0;
	}else
	{
		return 1;
	}
}
/**
 * @brief add a value at the last position the head points to in the ring buffer
 * 
 * @param buffer_ring pointer to a structure like ring_buffer_t
 * @param add_data value to be stored in the ring buffer
 * @retval none
 */
void ring_buffer_append(ring_buffer_t *buffer_ring, uint8_t add_data)
{
	//agregamos el dato al buffer
	buffer_ring->buffer_source[buffer_ring->head]=add_data;
	//aumentamos el contador ciclico
	buffer_ring->head=(buffer_ring->head + 1)% buffer_ring->max_size;
	// si el buffer ya estaba lleno anterior mente movemos la cola a la siguiente posicion
	if (buffer_ring->full==1)
	{
		buffer_ring->tail=(buffer_ring->tail +1)% buffer_ring->max_size;
	}
	//comprobamos si el ring buffer ya se lleno
	if (buffer_ring->head==buffer_ring->tail)
	{
		buffer_ring->full=1;
	}
}





