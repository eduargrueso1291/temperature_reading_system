/**
 *@file firmware_version.h
 *@date 24/05/2023
 *@author eduar grueso
 *@version 1.1
 */
#ifndef INC_FIRMWARE_VERSION_H_
#define INC_FIRMWARE_VERSION_H_
/*Exported defines********************************/
#define FW_VERSION "1.2.20230610"
/*Exported types**********************************/
/*Exported constans*******************************/
/*Exported macro**********************************/
/*Exported functions******************************/
#endif /* INC_FIRMWARE_VERSION_H_ */
