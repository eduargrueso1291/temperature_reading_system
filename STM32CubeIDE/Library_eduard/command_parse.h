/**
 *@file command_parse.h
 *@date 24/05/2023
 *@author eduar grueso
 *@version 1.1
 *@brief This file is responsible for the analysis of
 *commands and the execution of tasks as well
 */

#ifndef INC_COMMAND_PARSE_H_
#define INC_COMMAND_PARSE_H_
/*Exported defines********************************/
#define COMMAND_SET 'S'
#define COMMAND_GET 'G'
#define COMMAND_SPEED 'S'
#define COMMAND_TEMPERATURE 'T'
#define COMMAND_DOOR 'D'
#define COMMAND_TIME 'U'
#define COMMAND_VERSION 'F'
#define COMMAND_INVALID "NACK"
#define COMMAND_ACEPT "ACK"
#define COMMAND_HEATER 'H'
#define COMMAND_REPLY_TEMPERATURE 'T'
#define COMMAND_REPLY_HEATER 'H'
#define COMMAND_REPLY_SPEED 'F'
#define COMMAND_REPLY_DOOR 'D'
#define COMMAND_REPLY_TIME 'C'
#define COMMAND_REPLY_VERSION 'V'
#define DOOR_STATE_OPEN 0
#define DOOR_STATE_CLOSE 1
#define HEATER_STATE_ON 1
#define HEATER_STATE_OFF 0
/*Exported types**********************************/
/*Exported constans*******************************/
/*Exported macro**********************************/
/*Exported functions******************************/
void set_ccr_pwm(uint8_t, uint8_t);
void set_pwm_cooler(uint8_t);
uint8_t read_speed_of_the_command(ring_buffer_t *);
void set_command_execution(ring_buffer_t *, uint8_t);
void get_command_execution(ring_buffer_t *, uint8_t);
void command_parse(ring_buffer_t *);
void command_parse_set_tim_pwm(TIM_TypeDef *, uint8_t );

extern TIM_TypeDef *g_tim_pwm;
extern uint8_t g_chanel_pwm;
#endif /* INC_COMMAND_PARSE_H_ */
